'use strict';

class Bet {
	constructor() {
		this.stake = Math.floor(Math.random() * 20);
		this.disputed = false;
	}

	getStake() {
		return this.stake;
	}

	setDisputed(disputed) {
		this.disputed = disputed;
	}
}

class Moderator {
  	constructor(id) {
		this.id = id;
    	this.balance = Math.floor(Math.random() * 10000)
    	this.earnings = 0;
  	}

  	getBalance() {
		return this.balance;
	}

	getEarnings() {
		return this.earnings;
	}

  	updateBalance(newBalance) {
    	this.balance = newBalance;
  	}
}

class StakingContract {
	constructor() {
		this.stakeMin = 30;
	    this.stakes = [];
  	}

  	stake(day, moderator, amount) {
  		var stake = this.stakes.find(s => s.id === moderator.id);
  		if(stake) {
  			return false; // user has already staked
  		}
  		this.stakes = [...this.stakes, {
  			id: moderator.id,
  			amount: amount,
  			stakedDay: day,
  			until: day + this.stakeMin
  		}];
  		moderator.updateBalance(moderator.getBalance() - amount);
  	}

  	withdrawStake(day, moderator) {
  		var stake = this.stakes.find(s => s.id === moderator.id);
  		if(!stake) {
  			return false; // stake does not exist
  		}
  		if(day < stake.until) {
  			return false; // stake less than expiry date
  		}
  		moderator.updateBalance(moderator.getBalance() + stake.amount);
  		this.stakes = this.stakes.filter(s => s !== stake);
  	}


}

angular.module('myApp.homepage', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: 'homepage/homepage.html',
    controller: 'HomepageCtrl'
  });
}])

.controller('HomepageCtrl', ['$scope', '$interval', function($scope, $interval) {

	$scope.users = 100;
	$scope.dailyUserGrowth = 0.05;
	$scope.bets = [];
	$scope.contract = new StakingContract();
	$scope.moderators = [];
	$scope.day = 1;
	$scope.intervalFunc = null;
	$scope.appEarnings = 0;
	$scope.totalDisputedBets = 0;
	$scope.moderatorEarnings = 0;
	$scope.moderatorCount = 10;

	/** Settings **/
	$scope.betsPerDay = 3 * $scope.users;
	$scope.disputedBetsPerDay = 0.2; 
	$scope.appBetFee = 0.02;
	$scope.moderatorFee = 0.01;

	$scope.stopRun = function() {
		if (angular.isDefined($scope.intervalFunc)) {
            $interval.cancel($scope.intervalFunc);
            $scope.intervalFunc = null;
        }
	}

	$scope.continueRun = function() {
		$scope.intervalFunc = $interval(function() {
			$scope.generateDay();
		}, 200);
	};

	$scope.generateDay = function() {
		$scope.day++;
		$scope.users += $scope.users * $scope.dailyUserGrowth;
		for(var moderator of $scope.moderators) {
			$scope.contract.stake($scope.day, moderator, moderator.getBalance());
			$scope.contract.withdrawStake($scope.day, moderator);
		}
		$scope.bets = [];
		for(var i = 1; i <= $scope.betsPerDay; i++) {
			var bet = new Bet();
			var disputeProb = Math.random();
			if(disputeProb <= $scope.disputedBetsPerDay) {
				bet.setDisputed(true)
				$scope.totalDisputedBets++;
				$scope.moderatorEarnings += $scope.moderatorFee * bet.getStake();
			}
			console.log($scope.appBetFee * bet.getStake());
			$scope.appEarnings += $scope.appBetFee * bet.getStake();
			$scope.bets.push(bet);
		}
	};

	$scope.initialiseModerators = function() {
		for(let i = 1; i <= $scope.moderatorCount; i++) {
			var moderator = new Moderator(i);
			$scope.moderators.push(moderator);
		}
	};
	$scope.initialiseModerators();

}]);